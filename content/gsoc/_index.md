---
title: GSoC
layout: GSoC
menu:
#   main:
#     name: GSoC
#     weight: 2
faq:
  - question: "Is KDE part of GSoC this year?"
    answer: "Google is the one choosing which organizations participate and there is no guarantee that KDE will be selected."
  - question: "Which project should I participate in?"
    answer: "That is up to you to decide! We want all contributors to be motivated and happy and to continue contributing to KDE as long as they can, so it is important to choose a project you like!"
  - question: "When should I start contributing?"
    answer: "As soon as you can. Unfortunately, we cannot mentor everyone we want, so there is a selection done among the different candidates. We will usually go with the ones we are sure about, those who have already contributed to KDE. If you desire a mentorship program to get started, KDE's program Season of KDE is a great opportunity to get involved with a project before submitting an application for GSoC."
  - question: "Is it a good idea to generate my proposal with ChatGPT or equivalent?"
    answer: "No. Mentors are smart and will see that you have put zero effort in creating your proposal. Your proposal will be ignored. We need to see that you are interested with both the project and KDE."
  - question: "Where are the project ideas?"
    answer: "Go to https://community.kde.org/GSoC/ and click on \"See ideas for <YEAR>\" at the very top of the page."
  - question: "Where are the GSoC rules?"
    answer: "https://summerofcode.withgoogle.com/"
---

Google Summer of Code is a program sponsored by Google to help developers start contributing to open source software.
You can learn more about the Google Summer of Code at https://summerofcode.withgoogle.com/.

KDE has been participating in the GSoC since the beginning in 2005, mentoring multiple projects every year. One of our highest achievements is having a beloved previous GSoC contributor become the KDE e.V. president!

FAQ
===
Here are the most common questions/answers for GSoC:
{{< faq name="faq" >}}
